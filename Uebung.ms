.TL
übung

.nr PS 10
.nr GROWPS 4
.nr PSINCR 1p

.SH 1
2.8
.SH 2
Algo:
.RS
.RE
.SH 3
main
.RS
	for v in nodes:
.RS
		for l in labels of v
.RS
			buildPath(l);
.RE
.RE
.RE

.SH 3
buildPath:
.IP
			if(path(l) exsists) continue;
.IP
			if(path(l->prev) not existant)
.RS
.IP
				buildPath(l->prev)
.RE
.IP
			path(l) = path(l->prev) + edge(l->prev->node, l->node)

.SH 2
Laufzeit:
.SH 3
main:
.EQ
O(|labels|) O(buildPath)
.EN

.SH 4
buildPath:
.EQ
O(|V|)
.EN

.SH 4
aber:
.PP
.B buildPath
benoetigt nur konstante Zeit, wenn es auf einem label bereits augerufen wurde und rekursiert nur, wenn der Path von
.I l->prev
noch nicht existiert.
.PP
immer wenn
.B buildPath
mehr als
.EQ
O(1)
.EN
Zeit braucht, (sagen wir
.I k
viel Zeit), dann wurde nicht nur der Path von
.I l
sondern auch der von den
.I k
Vorgängern von
.I l->node
im Path von
.I l
berechnet, weswegen diese später nicht mehr berechnet werden müssen.
.SH 4
insgesamt:
.EQ
O(|labels|)
.EN
