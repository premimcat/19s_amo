---
title: Übung
output: pdf-document
---

\tableofcontents

# 1
## 1.1.
$O(n^2)$ : $10n^2$, $n^2+n$

$O(n^2/\log n)$: $n^2/\log n$, $10n^2/\log n + n \log n$

$O(n)$: $100n$, $n^2/(n+\log n$) (dominiert von oben durch $n^2/2n$ und von unten durch $n^2/n$

## 1.2

### a)
$\omega(f) \subset \Omega(f)$ da Def D,E, $f$ in gross omega aber nicht in klein

### b)
in C: $c=c_D c'=c_B$

### c)
s. Def (das gleiche, aber in die andere Richtung jeweils)

### d)
$c_D = 1/c_B$


## 1.3.
### a)
Kreis der Länge $2$:

$M={a,b} R={(a,b),(b,a)}$

sonst:

asd [^1]
[^1]: das ~ist~ _meine_ *fussnutte* 


Angenommen es gäbe einen Kreis $C={a_1, ... , a_|C|} \subseteq M$ im Hasse-Diagram.  
Da Relexivität $(a_i,a_j) \forall i != j$. also ungerichtete clique. Dann gibt es aber keine der Kanten.

### b)
Angenommen:

Sei o.B.d.A. $(a,b)$ und $(a,c)$ in $R$. Da Vollständigkeit entweder $(c,b)$ oder $(b,c) \in R$.

### c)
Knoten mit ${\it indeg}=0$


## 1.4.
1. Sort $M$ lexicographic.
2. Let $a$ ($b$) be the first (second) vector
3. While not at end of $M$
    + If $a$ dom $b$:
        - delete $b$ and let $b++$
        - $a=b$, $b++$

1 needs $O(n \log n)$

2 $O(1)$

3: As $b++$ in each loop: $O(n)$ loops, in each loop $O(1)$ $\Rightarrow$ $O(n)$

$\Rightarrow$ $O(n \log n)$

Wenn sortiert: $O(n)$

## 1.5.
### MOST
same graph as for MOSP

the $(0,0)$ edges are always in the MST. one of the other two edges of each triangle is in the MST.

Same reasoning as for MOSP

### MOTSP

circle of these gadgets. all edges not shown are $(2^n,2^n)$, so no sensible tsp tour uses any edge but the ones shown

same reasoning

## 1.6.
### a)
Da $|s| <= {\it poly}(|I|)$, daher höchstens $2^p(|I|)$ Möglichkeiten

### b)
$v$ in ${\it poly}(|I|, |S(I)|)$ berechenbar

Jedes $s$ in $S(I)$ ist höchstens poly in $|I|$ lang  $\Rightarrow$ $v$ in ${\it poly}(|I|)$ berechenbar

Da Ausgabe in poly, auch lange der Ausgabe in poly $\Rightarrow$ $v$ höchstens poly lang/ nachkommastellen


Für mehr Kriterien: $d =$ anz kriterien
a) $|W| <= 2^p(|x|)$
b) $W \subseteq [2^-q(|x|), 2^q(x)]^d$



## 1.7.
Hamilton Kreis

Zweite Kriterium gleich erstes Kriterium gleich Eingabe Kriterium

Bumm




\pagebreak
# 2
## 2.1.
Alle Zähler mit KGV aller Nenner multiplizieren.

Codierung vorher: Max (|zahler| + |nenner|)
Codierung jetzt: max(|zahler| * |kgv nenner|)

KGV Nenner <= max(|nenner|) * |S(I)| $\Rightarrow$ beides poly in |l|

**!Begründungen für Eigenschaften fehlen!**

## 2.2.
### a)
$O(n)$

### b)
$O(\log n)$

### c)
Turing Maschine muss bei jeder Ausgabe über alle bisherigen Ausgaben gehen. Es ist nicht moeglich eine Konfiguration nur auszugeben, wir müssen sie alle "nebeneinander " schreiben

**!Sprünge nicht möglich!**


## 2.3
$k’ = 2k$

Für gegebenes $n,m$ ist entweder $n <= m$ oder $m <(=) n$, damit ist entweder $nm <= n^2$ oder $nm <= m^2$

## 2.4
${\bf DelayP} \subseteq {\bf incP}$:

Wenn $k$-ter daelay ${\it poly}(|I|)$ ist, dann auch ${\it poly}(k + |I|)$

${\bf IncP} \subseteq {\bf OP}$:

Gesamtlaufzeit: ($h := |{\it output}|$)

$\sum_{0 <= k <= h} {\it poly}(k + |I|) <= h * {\it poly}(h + |I|) = {\it poly}(h + |I|)$

## 2.5
main:

* sortiere $M$
* Perms := $\emptyset$
* für jede Zahl $z$ aus $M$:
	* bilde sämtliche Permutationen der Elemente die $z$ sind
	* hänge an jede Teilpermutation aus Perms jede gebildete Permution an
* gebe Perms aus


Laufzeit:

* main:
	* $O(|M| \log |M|)$ fürs sortieren
	* höchstens $|M|$ Zahlen
	* in jedem Durchlauf der Forschleife werden die Permutationen nicht weniger
	* der letzte Durchlauf dauert $|$Perms$|$ lange
	* $\Rightarrow O(|M| \log |M| + |$Perms$|\cdot |M|)$



## 2.6
### a)
Gegeben eine Instanz, welche jener aus der Vorlesung im Beweis der *Intractability* von MOSP verwendeten entspricht, wobei ein weiterer Knoten, welcher eine Kante zu $s$ hat hinzugefügt wird.

Die Ausgabe von einem *MOSP* Algo mit $s$ als Startknoten und dem neuen Knoten als Endknoten hat konstante Grösse, da die Kante der einzige Weg zwischen $s$ und dem neuen Knoten ist.

Da aber der Rest der Instanz beliebig gross sein kann und *MOSSSP*, per Konstruktion $O(2^{|V|})$ viele Lösungen haben kann, welche alle ausgegeben werden müssen, ist die Aussage hiermit wiederlegt.


### b)
Die Ausgabe vom *MOSSSP* Algo ist $\cup_{v \in V} \mathcal{Y}_v$.
Ein ausgabesensitiver Algo für *MOSP*$_v$ benötigt $O(poly(|G|) + poly(|\mathcal{Y}_v|))$ viel Zeit.
Wenn wir für jedes $v \in V$ den *MOSP* Algo aufrufen ist die gesamt Laufzeit: $\sum_{v\in V} (poly(|G|) + poly(|\mathcal{Y}_v|)) = |V|\cdot poly(|G|) + poly(|\cup_{v \in V} \mathcal{Y}_v|)$, da $\mathcal{Y}_v \cap \mathcal{Y}_w = \emptyset$ für $v \neq w$.

Esta un ausgabesensitiver Algo.

## 2.7

## 2.8
### Algo:
* for label $l$ in $\cup_{v \in V} P_v$
	+ print "path of $l$:$
	+ label curr = $l$
	+ while node(curr->prev) != $s$)
		- print "edge (node(curr), node(curr->prev)"
		- curr = curr->prev
	+ print "end of path"

### Laufzeit:
$O(h)$, wobei $h$ die Laenge der Ausgabe ist, bzw. $O(n\cdot\mathcal{Y}_N)$ oder auch $O(\sum_{y\in \mathcal{Y}_N} p_y)$

\pagebreak
# 3
## 3.1 Antikette
### a
$y = (3, 3, 3)$  
$M= \{(1, 2, 2), (2, 1, 4)\}$  

$y^V = (2, 1, 4)$ dominiert $y$ nicht, da es grösser in der dritten Komponente ist.  
$(1, 2, 2)$ dominiert $y$ aber. 

### b 
$M=\{(1, 2, 3), (2, 3, 1), (3, 1, 2)\}$, $y=(2, 2, 2)$

## 3.2 Alternative Ordung LS
Summe aller Komponenten.  
Beweis komplett analog mit $\leq_{sum}$ statt $\leq_{lex}$

## 3.3 Anwendung

## 3.4 LS Heuristische Suche
Für jedes Label $\ell$, welches ein (in)direkter Vorgänger eines Labels $\ell^*$ aus $P_v$ ist, ist $y_{\ell} + u(v_{\ell}) \leq y_{\ell^*} + u(v_{\ell^*})$.
Dies ist der Fall, da $u(v_{\ell^*}) \leq u(v_{\ell}) + c(p_{v_{\ell},v_{\ell^*}})$, wobei $p_{v_{\ell},v_{\ell^*}}$ der Pfad von $v_{\ell}$ zu $v_{\ell^*}$ ist, welcher aus den entsprechenden Vorgänger von $v_{\ell^*}$ bezüglich $\ell^*$  besteht,
da $c(p_{v_{\ell},v_{\ell^*}})$ eine obere Schranke für die untere Schranke der Entferung von $v_{\ell}$ und $v_{\ell^*}$ und diese, zusammen mit $u(v_{\ell^*})$, aufgrund der Dreiecksungleichung eine obere Schranke für $u(v_{\ell})$ ist.

Mit dieser Beobachtung ist der Beweis analog zur Vorlesung.

## 3.5 Heuristische Suche $t$-Dominanz
Sidenote: $\leq_p$ ist das geschwungende Kleinergleich  

Angenommen dies wäre nicht der Fall. Sei $l^+=(y^+, t)$ ein solcher $s-t$-Pfad. Seien $y^* = y^+-y$ die Kosten des Teil von $l^+$, der durch den Abschnitt von $v$ zu $t$ zustande kommt. $y^+$ ist in jeder Komponente mindestens so gross wie $u(v)$, da Def. $u$, also $y + u(v) \leq_p y^+$. Da jedoch $y'\leq_p y+ u(v)$ und $y + u(v)$

## 3.6 Tree-Deletion
<!-- ![Minmalinstanz](3.6.png) -->
Die altrosa Linien zeigen an, aus welchem Label ein anderes entstanden ist.  
Das Label $(4,4)$ an $3$ wird erweitert, nachdem das Label $(2, 2)$ an $2$ hinzugefuegt wurde, welches das Label $(3, 3)$ aus dem $(4, 4)$ entstanden ist dominiert.

Dieses Beispiel fasst "mehrere" als "$>0$" auf und "erweitern", wenn ein Label versucht wird zu erweitern, wobei Erfolg keine Rolle spielt. Beide Annahmen können getroffen werden, da die Erweiterung des Beispiels, sodass die Begriffe strenger verstanden werden können trivial ist (alles um $s$ herum mehrfach kopieren bzw. ein weiterer Knoten samt Kante an $3$ dran).

## 3.7 KP Dec
NP-Membership ist trivial.

NP-Hardness per Cook-Reduktion auf KP$^{\textsc{ Dec}}$

Wenn es nicht-positive Gewichte gibt, dann wird auf jedes Gewicht eine Konstante $k_{c_{1, 2}} := 1 - \min_{i\in [n]} c_{1, 2}(i)$ addiert, sodass alle Gewichte $>0$ sind. Dann wird das Problem mit $k_1'=k_1+l\cdot k_{c_1}$ und $k_2'=k_2+l\cdot k_{c_2}$ für $1 < l < n$ gelöst. Auserdem wird geprüft, ob die leere Lösung und die Lösung mit allen Items für das Ausgangsproblem eine gültige Lösung sind.

Wenn $k_2 = \sum_{i\in [n]}c_2(i)$, dann ist nur die Lösung mit allen Items als mögliche Lösung zu prüfen.

Wenn $k_1 < 0$ oder $k_2 > \sum_{i\in [n]}c_2(i)$ (oBdA positive Gewichte, da s.o.), dann ist die Instanz nicht lösbar.  
Wenn $0 \leq k_1 \leq 1$, dann sind triviale Fälle zu prüfen (leere Lösung, Lösung mit einem Element)

Bei nicht-positiven Gewichten ist die Transformation der Gewichte in $O(n)$ möglich.
Ein Algo für KP$^{\textsc{Dec}}$ muss $O(n)$ mal aufgerufen werden.

Für nicht passende $k_{1, 2}$ ist das Problem in $O(n^2)$ lösbar (hōchstens $O(n)$ viele Checks von trivialen Lösungen, die jeweils höchstens $O(n)$ gross sind)


## 3.8 MOSP NP-hard
Cook-Reduktion:  
Rufe MOSP-Algo auf der Instanz auf. Wenn $|\mathcal{Y}_N| >2$ gebe "ja" zurück, sonst gebe "nein" zurück.  
Dass das ganze poly ist, ist trivial.
